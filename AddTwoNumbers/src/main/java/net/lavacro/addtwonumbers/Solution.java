package net.lavacro.addtwonumbers;

public class Solution {
	public static void main(String args[]) {
		ListNode l1 = new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(9))));
		ListNode l2 = new ListNode(9, new ListNode(9));

		ListNode sum = new ListNode();
		ListNode ptr1 = l1, ptr2 = l2, sumptr = sum;

		int carry = 0;

		while(true) {
			sumptr.val = (ptr1 == null ? 0 : ptr1.val) + (ptr2 == null ? 0 :ptr2.val) + carry;
			if(sumptr.val >= 10) {
				sumptr.val -= 10;
				carry = 1;
			} else {
				carry = 0;
			}

			if(ptr1 != null) ptr1 = ptr1.next;
			if(ptr2 != null) ptr2 = ptr2.next;

			if(ptr1 == null && ptr2 == null) {
				if(carry == 1) {
					sumptr.next = new ListNode(1);
				}
				break;
			}

			sumptr.next = new ListNode();
			sumptr = sumptr.next;
		}

		sumptr = sum;
		while(sumptr != null) {
			System.out.println(sumptr.val);
			sumptr = sumptr.next;
		}
	}
}
