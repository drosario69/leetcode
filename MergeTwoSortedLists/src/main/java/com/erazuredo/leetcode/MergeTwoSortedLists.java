package com.erazuredo.leetcode;

public class MergeTwoSortedLists {
	public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
		ListNode node, ptr = null;
		ListNode head = null;
		while(!(list1 == null && list2 == null)) {
			node = smallest(list1, list2);
			if(node == list1) {
				node = new ListNode(list1.val, null);
				list1 = list1.next;
			} else {
				node = new ListNode(list2.val, null);
				list2 = list2.next;
			}

			if(head == null) {
				head = node;
				ptr = node;
			} else {
				ptr.next = node;
				ptr = node;
			}
		}
		return head;
	}

	private ListNode smallest(ListNode list1, ListNode list2) {
		if(list2 == null) {
			return list1;
		}

		if(list1 == null) {
			return list2;
		}

		if(list1.val <= list2.val) {
			return list1;
		} else {
			return list2;
		}
	}
}
