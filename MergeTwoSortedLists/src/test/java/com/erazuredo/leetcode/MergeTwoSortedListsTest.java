package com.erazuredo.leetcode;

import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class MergeTwoSortedListsTest {
	@Test
	public void test1() {
		// 1 2 4
		// 1 3 4
		ListNode list1 = new ListNode(
				1, new ListNode(
						2, new ListNode(
								4, null
						)
				)
		);

		ListNode list2 = new ListNode(
				1, new ListNode(
				3, new ListNode(
				4, null
		)
		)
		);

		ListNode list3 = new MergeTwoSortedLists().mergeTwoLists(list1, list2);
		assertThat(list3.val).isEqualTo(1);
		list3 = list3.next;
		assertThat(list3.val).isEqualTo(1);
		list3 = list3.next;
		assertThat(list3.val).isEqualTo(2);
		list3 = list3.next;
		assertThat(list3.val).isEqualTo(3);
		list3 = list3.next;
		assertThat(list3.val).isEqualTo(4);
		list3 = list3.next;
		assertThat(list3.val).isEqualTo(4);
		list3 = list3.next;
		assertThat(list3).isNull();
	}
}
