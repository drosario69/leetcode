# Merge Two Sorted Lists

You are given the heads of two sorted linked lists `list1` and `list2`.

Merge the two lists in a one **sorted** list. The list should be made by splicing together the nodes of the first two lists.

Return _the head of the merged linked list_.



**Example 1:**

![merge_ex1](images/merge_ex1.jpeg)

```
Input: list1 = [1,2,4], list2 = [1,3,4]
Output: [1,1,2,3,4,4]
```

**Example 2:**

```
Input: list1 = [], list2 = []
Output: []
```

**Example 3:**

```
Input: list1 = [], list2 = [0]
Output: [0]
```


**Constraints:**

- The number of nodes in both lists is in the range `[0, 50]`.
- `-100 <= Node.val <= 100`
- Both `list1` and `list2` are sorted in **non-decreasing** order.

## Solution

There is not too much mystique over how to solve this: compare a value from one list
with a value from another list and populate a new list.

Some optimizations can be done when one of the two lists has reached the end. For example:

```text
list 1:
1, 2, 4, 9, 12, 15, 19, 25, 30

list 2:
3
```
Once the new list contains `1, 2, 3`, we can simply copy the remainder of list1 without making comparisons.

The non-optimized results (I had evidently completed this before):

![solution](images/solution.png)
