package com.erazurdo.leetcode;

import java.util.Map;
import java.util.HashMap;

public class TopKFrequentElements {
	public int[] topKFrequent(int[] nums, int k) {
		Map<Integer, Integer> freq = new HashMap<>();
		for(int num: nums) {
			Integer t = freq.get(num);
			if(t == null) {
				freq.put(num, 1);
			} else {
				freq.replace(num, t + 1);
			}
		}

		System.out.println(freq);
		return nums;
	}
}
