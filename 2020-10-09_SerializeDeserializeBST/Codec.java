import java.util.ArrayList;
import java.util.List;

public class Codec {
	private final List<String> arr = new ArrayList<>();

	private void recurse(TreeNode.Node ptr) {
		arr.add(String.valueOf(ptr.val));
		if(ptr.left != null) {
			recurse(ptr.left);
		}
		if(ptr.right != null) {
			recurse(ptr.right);
		}
	}

	// Encodes a tree to a single string.
	public String serialize(TreeNode root) {
		if(root.root != null) {
			recurse(root.root);
		}
		return "[" + String.join(",", arr) + "]";
	}

	// Decodes your encoded data to tree.
	public TreeNode deserialize(String data) {
		TreeNode tree = new TreeNode();
		String nums = data.substring(1, data.length() - 1);
		if(nums.length() > 0) {
			for (String num : nums.split(",")) {
				tree.add(Integer.valueOf(num));
			}
		}
		return tree;
	}
}

/**
 * Submitted response:
 * 
 * public class Codec {
 * 	private final List<String> arr = new ArrayList<>();
 *
 * 	private void recurse(TreeNode ptr) {
 * 		arr.add(String.valueOf(ptr.val));
 * 		if(ptr.left != null) {
 * 			recurse(ptr.left);
 *                }
 * 		if(ptr.right != null) {
 * 			recurse(ptr.right);
 *        }* 	}
 *
 * 	// Encodes a tree to a single string.
 * 	public String serialize(TreeNode root) {
 *         if(root != null) {
 * 		    recurse(root);
 *         }
 * 		return "[" + String.join(",", arr) + "]";* 	}
 *
 *     private void rebuild(TreeNode tree, int val) {
 *         if(val < tree.val) {
 *             if(tree.left == null) {
 *                 tree.left = new TreeNode(val);
 *             } else {
 *                 rebuild(tree.left, val);
 *             }
 *         } else {
 *             if(tree.right == null) {
 *                 tree.right = new TreeNode(val);
 *             } else {
 *                 rebuild(tree.right, val);
 *             }
 *         }
 *     }
 *
 *     // Decodes your encoded data to tree.
 * 	public TreeNode deserialize(String data) {
 * 		TreeNode tree = null;
 * 		String nums = data.substring(1, data.length() - 1);
 *         if(nums.length() > 0) {
 *             String[] vals = nums.split(",");
 *             for(String val: vals) {
 *                 if(tree == null) {
 *                     tree = new TreeNode(Integer.valueOf(val));
 *                 } else {
 *                     rebuild(tree, Integer.valueOf(val));
 *                 }
 *             }
 *         }
 * 		return tree;* 	}
 * }
 */