public class SerDeserBST {
	public static void main(String[] args) {
		TreeNode root = new TreeNode();
		root.add(2);
		root.add(1);
		root.add(3);
		root.disp();

		Codec ser = new Codec();
		Codec deser = new Codec();
		String tree = ser.serialize(root);
		System.out.println("tree: " + tree);
		TreeNode ans = deser.deserialize(tree);
		ans.disp();

		TreeNode blank = new TreeNode();
		blank.disp();
		Codec test = new Codec();
		tree = test.serialize(blank);
		System.out.println("tree: " + tree);
		Codec untest = new Codec();
		ans = untest.deserialize(tree);
		ans.disp();
	}
}

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */

/**
 * Your Codec object will be instantiated and called as such:
 * Codec ser = new Codec();
 * Codec deser = new Codec();
 * String tree = ser.serialize(root);
 * TreeNode ans = deser.deserialize(tree);
 *return ans;
 */
