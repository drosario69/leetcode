public class TreeNode {
	Node root = null;
	public static class Node {
		Node left;
		Node right;
		int val;
	}

	public void add(int newval) {
		Node node = new Node();
		node.val = newval;

		if(root == null) {
			root = node;
			return;
		}

		Node ptr = root;

		// don't need recursion for this
		while(true) {
			if(newval < ptr.val) {
				if(ptr.left == null) {
					ptr.left = node;
					break;
				} else {
					ptr = ptr.left;
				}
			} else {
				if(ptr.right == null) {
					ptr.right = node;
					break;
				} else {
					ptr = ptr.right;
				}
			}
		}
	}

	private void showTree(Node ptr, int depth) {
		if(ptr.right != null) {
			showTree(ptr.right, depth + 1);
		}
		System.out.println("--".repeat(depth) + ptr.val);
		if(ptr.left != null) {
			showTree(ptr.left, depth + 1);
		}
	}

	void disp() {
		if(root != null) {
			showTree(root, 0);
		}
	}
}
