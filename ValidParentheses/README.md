# Valid Parentheses

Given a string `s` containing just the characters `'('`, `')'`, `'{'`, `'}'`, `'['` and `']'`, determine if the input string is valid.

An input string is valid if:

1. Open brackets must be closed by the same type of brackets.
1. Open brackets must be closed in the correct order.

__Example 1:__

```
Input: s = "()"
Output: true
```

__Example 2:__

```
Input: s = "()[]{}"
Output: true
```

__Example 3:__

```
Input: s = "(]"
Output: false
```
 

__Constraints:__

```
1 <= s.length <= 10^4
s consists of parentheses only '()[]{}'.
```

## Solution

This question screams "stack". When writing an interpreter or a compilier, parsing tokens is done using a stack. We will leverage the stack class in the STL for this solution.

The logic is fairly simply: push an "open" character onto the stack and when we find a "closing" character, inspect the stack to determine if the corresponding opening character is on the stack.

The runtime statistics are as follows:

![speed](images/speed.png)

![memory](images/memory.png)
