#include <stack>
#include <unordered_map>
#include <iostream>

using namespace std;

unordered_map <char, char> parens = { {'}','{'}, {')','('}, {']','['} };

bool isValid(string s) {
	// using a loop so I can break out prematurely if mismatch
	stack<char> stk;
	for(char &item: s) {
		if(parens[item]) {	// the key has a value, meaning it's a closing paren
			if(stk.empty()) {
				return false;
			}
			if(stk.top() != parens[item]) { // does the stack have the reciprocal?
				return false;
			}
			stk.pop();
		} else {
			stk.push(item);
		}
	}
	if(!stk.empty()) {
		return false;
	}
	return true;
}

int main(void) {
	cout << isValid("()") << endl;
	cout << isValid("()[]{}") << endl;
	cout << isValid("(]") << endl;
	return 0;
}
