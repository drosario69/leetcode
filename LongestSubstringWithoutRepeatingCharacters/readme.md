# Longest Substring Without Repeating Characters

## Problem:

Given a string `s`, find the length of the **longest substring** without repeating characters.

**Example 1:**

```text
Input: s = "abcabcbb"
Output: 3
Explanation: The answer is "abc", with the length of 3.
```

**Example 2:**
```text
Input: s = "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.
```

Example 3:
```text
Input: s = "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3.
Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.
```

**Constraints:**

- `0 <= s.length <= 5 * 10^4`
- `s` consists of English letters, digits, symbols and spaces.

---

## Solution

The approach used here is known as a _sliding window_. Let's consider the data set:

```text
abcabcbb
```

We initially have a range of 2: `a` and `b`, and none of the characters repeat. We then examine a larger range, which is `a`, `b`, `c`, and they are still all unique. However, once the right index becomes 3, the character is `a`, so we have a repeat. At this point, we have attained a maximum of 3 unique characters, so we don't want to consider any substrings shorter than 3 characters. When we examine the 4th character, which is `a`. We already have that, so we shift the entire window over to examine the next 4 characters:

```
abcabcbb
|__|

abcabcbb
 |__|
```

We continue sliding the window, which we eventually exhaust:

```
abcabcbb
  |__|

abcabcbb
   |__|

abcabcbb
    |__|
```

In no case do we find 4 characters that are unique.

### Data structures

We could conceivably store the uniq set of characters in a list, but the access time for elements is linear, not constant. If we use a HashSet, we get constant access time, making it a more efficient data structure.

One optimization was implemented: converting the source string to a character array, since the access time for each element in an array is O(1) while .charAt() is not O(1).

![results](images/LongestSubstring.png)

## C++

C++ is a natural fit for sliding windows. Anything involving pointers or references is easily accomplished in C++, so I included a C++ solution. Simply run "make" followed by "./solution".

![results](images/solution_c++.png)

---
## Optimizations

Although I have already discussed an optimization, there is another important optimization: using a character array rather than a HashSet (or unordered_set in C++);
Despite the speed of the HashSet/unordered_set, there is nothing faster than a raw character array. Values are accessed quickly via indices, and we can use values of 0 and 1 to indicate whether the value is present.

The C++ solution was updated with this optimization and ranked significantly better in runtime execution speed once submitted.

The Java solution was updated with this optimization, but instead of a character array, a byte array is required. Character arrays represent multibyte characters and are not suitable for this problem.
