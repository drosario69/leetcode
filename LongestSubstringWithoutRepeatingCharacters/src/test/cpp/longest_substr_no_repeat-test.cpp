#include <iostream>
#include <string>
#include <cassert>
#include "../../main/cpp/longest_substr_no_repeat.cpp"

using namespace std;

int main() {
	Solution solution;

	string s = "abcabcbb";
	assert(solution.lengthOfLongestSubstring(s) == 3);
	cout << "Test passed for '" << s << "'" << endl;

	s = "bbbbb";
	assert(solution.lengthOfLongestSubstring(s) == 1);
	cout << "Test passed for '" << s << "'" << endl;

	s = "pwwkew";
	assert(solution.lengthOfLongestSubstring(s) == 3);
	cout << "Test passed for '" << s << "'" << endl;

	return 0;
}
