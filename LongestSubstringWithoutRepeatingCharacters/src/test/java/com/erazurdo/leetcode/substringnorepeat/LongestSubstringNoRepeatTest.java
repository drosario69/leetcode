package com.erazurdo.leetcode.substringnorepeat;

import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class LongestSubstringNoRepeatTest {
	@Test
	public void test1() {
		String s = "abcabcbb";
		LongestSubstringNoRepeat solution = new LongestSubstringNoRepeat();
		assertThat(solution.lengthOfLongestSubstring(s)).isEqualTo(3);
	}

	@Test
	public void test2() {
		String s = "bbbbb";
		LongestSubstringNoRepeat solution = new LongestSubstringNoRepeat();
		assertThat(solution.lengthOfLongestSubstring(s)).isEqualTo(1);
	}

	@Test
	public void test3() {
		String s = "pwwkew";
		LongestSubstringNoRepeat solution = new LongestSubstringNoRepeat();
		assertThat(solution.lengthOfLongestSubstring(s)).isEqualTo(3);
	}
}
