package com.erazurdo.leetcode.substringnorepeat;

public class LongestSubstringNoRepeat {
	public int lengthOfLongestSubstring(String s) {
		if(s.length() < 2) return s.length();

		byte[] vals = new byte[128];
		byte[] ch = s.getBytes();
		int left = 0, right = 1, max = 1;
		vals[ch[0]] = 1;

		while(right < ch.length) {
			if(vals[ch[right]] > 0) {
				--vals[ch[left++]];
			} else {
				++vals[ch[right++]];
				max = Math.max(right - left, max);
			}
		}
		return max;
	}
}
