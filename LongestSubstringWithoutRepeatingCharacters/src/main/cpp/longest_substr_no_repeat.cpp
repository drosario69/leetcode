#ifndef __SOLUTION__
#define __SOLUTION__ 1

#include <string>
#include <cstring>

using namespace std;

class Solution {
public:
	int lengthOfLongestSubstring(string s) {
		if(s.length() < 2) return s.length();

		char vals[128];
		memset(vals, 0, 128);
		char *charr = &s[0];
		int left = 0, right = 1, len = s.length(), max = 1;
		vals[charr[0]] = 1;

		while(right < len) {
			if(vals[charr[right]] > 0) {
				// slide the window
				--vals[charr[left++]];
			} else {
				// slide the right side of the window
				++vals[charr[right++]];
				max = (right - left > max) ? right - left : max;
			}
		}
		return max;
	}
};

#endif
