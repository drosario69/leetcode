# Two Sum

Given an array of integers `nums` and an integer `target`, return *indices of the two numbers such that they add up to* `target`.

You may assume that each input would have __*exactly* one solution__, and you may not use the same element twice.

You can return the answer in any order.

__Example 1:__
```
Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
```
__Example 2:__
```
Input: nums = [3,2,4], target = 6
Output: [1,2]
```
__Example 3:__
```
Input: nums = [3,3], target = 6
Output: [0,1]
```
 

Constraints:

    2 <= nums.length <= 10^4
    -10^9 <= nums[i] <= 10^9
    -10^9 <= target <= 10^9
    Only one valid answer exists.


## Solution #1

The initial, obvious solution is just to have nested loops, which has the worst case scenario of *O((N-1) + (N-2) + (N-3) ... + 1)*. For a list with 5 elements, we can break it down as so:

|Iteration|loop|el1|el2|el3|el4|el5|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|1|1|x|x||||
|2|1|x||x|||
|3|1|x|||x||
|4|1|x||||x|
|5|2||x|x|||
|6|2||x||x||
|7|2||x|||x|
|8|3|||x|x||
|9|3|||x||x|
|10|4||||x|x|

Since *n* is `5`, our efficiency formula of *O((N-1) + (n-2) ... + 1)* indeed works: 4+3+2+1, which is 10.

## Solution #2

This can be inefficient of the list is very large. For 1,000,000 elements, our efficiency is 500,000,500,000. It will be more efficient to first sort the list and then iteratively split the list and compare. This requires a sorting algorithm with high efficiency, and the STL provides such an algorithm with an efficiency of *O(N·log(N))* (the base of the logarithm is 2):

https://en.cppreference.com/w/cpp/algorithm/sort

With our list of 1,000,000 elements, our efficiency becomes 13,815,511 ... 36,191 times more efficient.

After sorting, we then must effectively perform a binary search, so on top of *O(N·log(N))*, we must add *log(N)*. Even with the search, we are still more efficient than approach #1 in all but trivial cases:

|Items|Approach #1|Approach #2|
|--:|--:|--:|
|2|1|2|
|3|3|4|
|4|6|6|
|5|10|9|
|6|15|12|
|7|21|15|
|8|28|18|
|9|36|21|
|10|45|25|

