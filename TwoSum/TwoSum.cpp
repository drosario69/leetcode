#include <iostream>
#include <vector>

using namespace std;

vector<int> twoSum(vector<int>& nums, int target) {
	int outer, inner;
	vector<int> rv = {0,1}; // initialize so I don't have to grow later

	for(outer = 0; outer < nums.size() - 1; outer++) {
		for(inner = outer + 1; inner < nums.size(); inner++) {
			if(nums[outer] + nums[inner] == target) {
				rv[0] = outer;
				rv[1] = inner;
				return rv;
			}
		}
	}
	return rv;
}

int main(int argc, char* argv[]) {
	vector<int> nums = {2,7,11,15};
	int target = 9;

	vector<int> ex1 = twoSum(nums, target);

	cout << "[" << ex1[0] << "," << ex1[1] << "]" << endl;
	return 0;
}
