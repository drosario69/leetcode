package com.erazurdo.leetcode.lrucache;

import java.util.HashMap;
import java.util.Map;

public class LRUCache {
	private Node head;
	private Node tail;
	private int nodes;
	private final int capacity;

	private final Map<Integer, Node> nodeMap;

	LRUCache(int capacity) {
		nodes = 0;
		this.capacity = capacity;
		nodeMap = new HashMap<>();
	}

	int get(int key) {
		Node node = nodeMap.get(key);
		if(node == null) {
			return -1;
		} else {
			int rv = node.value;
			if(capacity > 1) {
				moveToTop(node);
			}
			return rv;
		}
	}

	void put(int key, int value) {
		// first check if exists
		Node node = nodeMap.get(key);	//instead of doing a .containsKey() AND .get(), just do one .get()
		if(node != null) {
			node.value = value;
			if(capacity > 1) { // no need to try to shuffle if capacity is 1 and key is found
				moveToTop(node);
			}
		} else if(nodes == capacity) {
			evictAndReplace(key, value);
		} else { // just add
			addNode(key, value);
		}
	}

	private void moveToTop(Node node) {
		// check neighbors to see if I need to shift
		if(node.prev != null) { // already at the top of the queue?
			// two scenarios: (1) this node is tail, which is an easy move, or (2) not a tail, so more pointers need adjusting
			Node prev = node.prev;		// keep track of these, ...
			Node next = node.next;		// otherwise I'll lose track of it

			// my previous has to point to my next
			prev.next = next;
			// my next has to point to my previous (if not null)
			if(next != null) {
				next.prev = prev;
			}

			// move our node to the head
			node.prev = null;
			// was I the tail?
			if(node.next == null) {
				tail = prev;
			}
			node.next = head;

			// head needs to point back to the new head
			head.prev = node;

			// update head
			head = node;
		}
	}

	private void addNode(int key, int value) {
		Node newNode = new Node();
		newNode.key = key;
		newNode.value = value;
		newNode.next = head; // ok if null since it effectively signifies the tail
		if(head != null) {
			head.prev = newNode;
		} else {
			tail = newNode;
		}
		head = newNode;
		nodeMap.put(key, newNode);
		nodes++;
	}

	private void evictAndReplace(int key, int value) {
		nodeMap.remove(tail.key);
		if(capacity == 1) { // special case
			// don't have to change any pointers or adjust counters; just keep same memory location
			nodeMap.put(key, head);
			head.key = key;
			head.value = value;
		} else {
			tail = tail.prev;
			tail.next = null;
			nodes--;
			addNode(key, value);
		}
	}
}

class Node {
	public Node prev;
	public Node next;
	public int key;
	public int value;
}
