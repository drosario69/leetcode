package com.erazurdo.leetcode.lrucache;

import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class LRUCacheTests {
	@Test
	public void test1() {
		LRUCache lRUCache = new LRUCache(2);
		lRUCache.put(1, 1); // cache is {1=1}
		lRUCache.put(2, 2); // cache is {2=2, 1=1}
		assertThat(lRUCache.get(1)).isEqualTo(1); // 1 becomes head {1=1, 2=2}

		lRUCache.put(3, 3); // LRU key was 2, evicts key 2, cache is {3=3, 1=1}
		assertThat(lRUCache.get(2)).isEqualTo(-1); // no change

		lRUCache.put(4, 4); // LRU key was 1, evicts key 1, cache is {4=4, 3=3}
		assertThat(lRUCache.get(1)).isEqualTo(-1); // no change

		assertThat(lRUCache.get(3)).isEqualTo(3); // 3 becomes head {3=3, 4=4}

		assertThat(lRUCache.get(4)).isEqualTo(4); // 4 becomes head {4=4, 3=3}
	}

	@Test
	public void test2() {
		/*
			Input:

			["LRUCache","put","put","put","put","get","get","get","get","put","get","get","get","get","get"]
			[[3],[1,1],[2,2],[3,3],[4,4],[4],[3],[2],[1],[5,5],[1],[2],[3],[4],[5]]

			Output:

			[null,null,null,null,null,4,3,2,-1,null,-1,2,3,-1,5]
		 */
		LRUCache lRUCache = new LRUCache(3);
		lRUCache.put(1, 1); // 1
		lRUCache.put(2, 2); // 2, 1
		lRUCache.put(3, 3); // 3, 2, 1

		lRUCache.put(4, 4); //
		assertThat(lRUCache.get(4)).isEqualTo(4);
		assertThat(lRUCache.get(3)).isEqualTo(3);
		assertThat(lRUCache.get(2)).isEqualTo(2);
		assertThat(lRUCache.get(1)).isEqualTo(-1);

		lRUCache.put(5, 5); //
		assertThat(lRUCache.get(1)).isEqualTo(-1);
		assertThat(lRUCache.get(2)).isEqualTo(2);
		assertThat(lRUCache.get(3)).isEqualTo(3);
		assertThat(lRUCache.get(4)).isEqualTo(-1);
		assertThat(lRUCache.get(5)).isEqualTo(5);
	}

	@Test
	public void test3() {
		//	["LRUCache","put","get","put","get","get"]
		//	[[1],[2,1],[2],[3,2],[2],[3]]
		LRUCache lRUCache = new LRUCache(1);

		lRUCache.put(2, 1);
		assertThat(lRUCache.get(2)).isEqualTo(1);

		lRUCache.put(3, 2);
		assertThat(lRUCache.get(2)).isEqualTo(-1);
		assertThat(lRUCache.get(3)).isEqualTo(2);
	}

	@Test
	public void test4() {
		/*
			Input:
			["LRUCache","get","get","put","get","put","put","put","put","get","put"]
			[[1],[6],[8],[12,1],[2],[15,11],[5,2],[1,15],[4,2],[5],[15,15]]

			Output:
			[null,-1,-1,null,-1,null,null,null,null,-1,null]
		 */
		LRUCache lRUCache = new LRUCache(1);
		assertThat(lRUCache.get(6)).isEqualTo(-1);
		assertThat(lRUCache.get(8)).isEqualTo(-1);

		lRUCache.put(12, 1);
		assertThat(lRUCache.get(2)).isEqualTo(-1);

		lRUCache.put(15, 11);
		lRUCache.put(5, 2);
		lRUCache.put(1, 15);
		lRUCache.put(4, 2);

		assertThat(lRUCache.get(5)).isEqualTo(-1);
		lRUCache.put(15, 15);
	}
}
