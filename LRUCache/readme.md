# LRU Cache

Design a data structure that follows the constraints of a **Least Recently Used (LRU) cache**.

Implement the `LRUCache` class:

- `LRUCache(int capacity)` Initialize the LRU cache with **positive** size `capacity`.
- `int get(int key)` Return the value of the key if the key exists, otherwise return `-1`.
- `void put(int key, int value)` Update the value of the `key` if the `key` exists. Otherwise, add the `key-value` pair to the cache. If the number of keys exceeds the `capacity` from this operation, **evict** the least recently used key.

The functions `get` and `put` must each run in `O(1)` average time complexity.


**Example 1:**

```
Input
["LRUCache", "put", "put", "get", "put", "get", "put", "get", "get", "get"]
[[2], [1, 1], [2, 2], [1], [3, 3], [2], [4, 4], [1], [3], [4]]
Output
[null, null, null, 1, null, -1, null, -1, 3, 4]

Explanation
LRUCache lRUCache = new LRUCache(2);
lRUCache.put(1, 1); // cache is {1=1}
lRUCache.put(2, 2); // cache is {1=1, 2=2}
lRUCache.get(1);    // return 1
lRUCache.put(3, 3); // LRU key was 2, evicts key 2, cache is {1=1, 3=3}
lRUCache.get(2);    // returns -1 (not found)
lRUCache.put(4, 4); // LRU key was 1, evicts key 1, cache is {4=4, 3=3}
lRUCache.get(1);    // return -1 (not found)
lRUCache.get(3);    // return 3
lRUCache.get(4);    // return 4
```

**Constraints:**

- `1 <= capacity <= 3000`
- `0 <= key <= 10^4`
- `0 <= value <= 10^5`
- At most `2 * 10^5` calls will be made to `get` and `put`.

## Solution

One approach for this is to use both a doubly linked list and a map so that we can get access to the nodes in about O(1).
An alternate solution is to use an array instead, thereby guaranteeing O(1), but that doesn't scale very well if the key is an unsigned long  of any value (Long.MAX_VALUE is 9,223,372,036,854,775,807).
A balance between speed and memory is to use the map, which will be slightly slower in speed but more practical.

Using the above operations, our cache looks like:

|List|1=1|2=2|get 1|3=3|get 2|4=4|get 1|get 3|get 4|
|---|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|head|1|2|1|3|3|4|4|3|4|
|tail| |1|2|1|1|3|3|4|3|

A read operation on an existing key makes it the most recent, pushing it to the top. A 2-node linked list makes it a little difficult to visualize the changes in the list, so let's try a 3-node list:

```
["LRUCache","put","put","put","put","get","get","get","get","put","get","get","get","get","get"]
[[3],[1,1],[2,2],[3,3],[4,4],[4],[3],[2],[1],[5,5],[1],[2],[3],[4],[5]]
```

|List|1=1|2=2|3=3|4=4|get 4|get 3|get 2|get 1|5=5|get 1|get 2|get 3|get 4|get 5|
|---|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|head|1|2|3|4|4|3|2|2|5|5|2|3|3|5|
|    | |1|2|3|3|4|3|3|2|2|5|2|2|3|
|tail| | |1|2|2|2|4|4|3|3|3|5|5|2|

Results:

![results](images/results.png)

Speed:

![speed](images/speed.png)

Memory:

![memory](images/memory.png)
