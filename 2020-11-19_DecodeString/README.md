## Decode String

Given an encoded string, return its decoded string.

The encoding rule is: `k[encoded_string]`, where the encoded_string inside the square brackets is being repeated exactly _k_ times. Note that _k_ is guaranteed to be a positive integer.

You may assume that the input string is always valid; No extra white spaces, square brackets are well-formed, etc.

Furthermore, you may assume that the original data does not contain any digits and that digits are only for those repeat numbers, _k_. For example, there won't be input like `3a` or `2[4]`.

 

**Example 1:**

```
Input: s = "3[a]2[bc]"
Output: "aaabcbc"
```

**Example 2:**

```
Input: s = "3[a2[c]]"
Output: "accaccacc"
```

**Example 3:**

```
Input: s = "2[abc]3[cd]ef"
Output: "abcabccdcdcdef"
```

**Example 4:**

```
Input: s = "abc3[cd]xyz"
Output: "abccdcdcdxyz"
```
 
**Constraints:**

- 1 <= s.length <= 30
- `s` consists of lowercase English letters, digits, and square brackets `'[]'`.
- `s` is guaranteed to be a **valid** input.
- All the integers in `s` are in the range `[1, 300]`.

---

## Solution

My approach involves breaking up the expression into 3 parts:

1. whatever comes before the first `[`
0. everything in between the first `[` and the last `]`
0. everything after the last `]`

I find the "coefficient" to the expression in `[]`, recurse into the "middle" expression and multiply the results by the coefficient, and then recurse into the tail section. The three are concatenated, and this generates the decoded string.
