package p20201119;

public class App {
	public static void main(String[] args) {
		Solution solution = new Solution();

		String s1 = "3[a]2[bc]";
		System.out.println("aaabcbc? " +solution.decodeString(s1));

		String s2 = "3[a2[c]]";
		System.out.println("accaccacc? " +solution.decodeString(s2));

		String s3 = "2[abc]3[cd]ef";
		System.out.println("abcabccdcdcdef? " +solution.decodeString(s3));

		String s4 = "abc3[cd]xyz";
		System.out.println("abccdcdcdxyz? " +solution.decodeString(s4));
	}
}
