package p20201119;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
	private final Pattern pattern = Pattern.compile("[0-9]{1,3}");

	public String decodeString(String s) {
		// extract expression in brackets, if any
		int pos = s.indexOf("[");
		if(pos != -1) {
			String pre = "", mid = "", post = "";
			int begin = pos;
			int bracket = 1;
			for(pos++; pos<s.length(); pos++) { // the initial pos++ starts me AFTER the "["
				if(s.charAt(pos) == ']') {
					if(--bracket == 0) {
						break;
					}
				} else {
					if(s.charAt(pos) == '[') {
						bracket++;
					}
				}
			}

			mid = s.substring(begin + 1, pos);

			// if we found brackets, then a number MUST precede them
			Matcher matcher = pattern.matcher(s.substring(0, begin));
			matcher.find();
			if (matcher.start() > 0) { // there is a string before the number
				pre = s.substring(0, matcher.start());
			}
			Integer num = Integer.valueOf(s.substring(matcher.start(), matcher.end()));

			if(s.length() - 1 > pos) {
				post = decodeString(s.substring(pos + 1));
			}
			return pre + decodeString(mid).repeat(num) + post;
		} else {
			return s;
		}
	}
}
