package p20201203;

public class Solution {
	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode() {}
		TreeNode(int val) { this.val = val; }
		TreeNode(int val, TreeNode left, TreeNode right) {
			this.val = val;
			this.left = left;
			this.right = right;
		}
	}

	private TreeNode newRoot, ptr;

	public TreeNode increasingBST(TreeNode root) {
		newRoot = null;
		if(root != null) {
			rebuild(root);
		}
		return newRoot;
	}

	private void rebuild(TreeNode node) {
		if(node.left != null) {
			rebuild(node.left);
		}

		if(newRoot == null) {
			newRoot = node;
			ptr = newRoot;
		} else {
			ptr.right = node;
			ptr = node;
			ptr.left = null;
		}

		if(node.right != null) {
			rebuild(node.right);
		}
	}

	public static void main(String[] args) {
		// 5,3,6,2,4,null,8,1,null,null,null,7,9
		TreeNode tree = new TreeNode(
			5,
			new TreeNode(
				3,
				new TreeNode(2,
					new TreeNode(1),
					null
				),
				new TreeNode(4)
			),
			new TreeNode(6,
				null,
				new TreeNode(
					8,
					new TreeNode(7),
					new TreeNode(9)
				)
			)
		);

		Solution solution = new Solution();
		TreeNode resp = solution.increasingBST(tree);
		System.out.println("done");
	}
}
