## The kth Factor of n

Given two positive integers `n` and `k`.

A factor of an integer `n` is defined as an integer `i` where `n % i == 0`.

Consider a list of all factors of `n` sorted in **ascending order**, return the `kth` factor in this list or return **-1** if `n` has less than `k` factors.

**Example 1:**

```
Input: n = 12, k = 3
Output: 3
Explanation: Factors list is [1, 2, 3, 4, 6, 12], the 3rd factor is 3.
```

**Example 2:**

```
Input: n = 7, k = 2
Output: 7
Explanation: Factors list is [1, 7], the 2nd factor is 7.
```

**Example 3:**

```
Input: n = 4, k = 4
Output: -1
Explanation: Factors list is [1, 2, 4], there is only 3 factors. We should return -1.
```

**Example 4:**

```
Input: n = 1, k = 1
Output: 1
Explanation: Factors list is [1], the 1st factor is 1.
```

**Example 5:**

```
Input: n = 1000, k = 3
Output: 4
Explanation: Factors list is [1, 2, 4, 5, 8, 10, 20, 25, 40, 50, 100, 125, 200, 250, 500, 1000].
```
 

**Constraints:**

- `1 <= k <= n <= 1000`

---

## Solution

The immediate solution that comes to mind when dealing with factors is simply to iterate from 1 to `n` and perform a modulus test.
The reality is that in order to find all the factors of a number, you only neet to test from 1 to the square root of `n`, and
`n` divided by any of the factors less than the square root of `n` provides the complementing factor. For example:

```
12 / 1 = 12
12 / 2 = 6
12 / 3 = 4
```

We can stop there because the square root of 12 is 3.46. Still not convinced? Look at the chart above. The quotients and divisors comprise the factors.
Taken together, you get:

`1, 2, 3, 4, 6, 12`

As your modulus test succeeds, store the quotients so that if the desired factor is not within the "low" factors (less than sqrt(n)), you can obtain it
from the "high" factors (complementary, greater than sqrt(n)).
