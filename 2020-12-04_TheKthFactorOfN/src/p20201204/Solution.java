package p20201204;

import java.util.ArrayList;
import java.util.List;

public class Solution {
	public int kthFactor(int n, int k) {
		if(k > n) {
			return -1;
		}

		List<Integer> factors = new ArrayList<>();
		int count = 0;

		for(int i = 1; i <= Math.sqrt(n); i++) {
			if(n % i == 0) {
				if(k == ++count) {
					return i;
				}
				int factor = n / i;
				if(factor == i) { // square root
					continue;
				}
				factors.add(factor);
			}
		}

		// if we got here, either there are no matching factors, or it's a "high" factor
		if(k > (count + factors.size())) { // no dice, and can't say "count * 2" because prime factors remove 1
			return -1;
		}

		// k - count
		int reverseIdx = k - count;
		return factors.get(factors.size() - reverseIdx);
	}

	public static void main(String[] args) {
		Solution solution = new Solution();
		int factor = solution.kthFactor(12, 3);
		System.out.println("3? " + factor);

		factor = solution.kthFactor(7, 2);
		System.out.println("7? " + factor);

		factor = solution.kthFactor(4, 4);
		System.out.println("-1? " + factor);

		factor = solution.kthFactor(1, 1);
		System.out.println("1? " + factor);

		factor = solution.kthFactor(1000, 3);
		System.out.println("4? " + factor);

		factor = solution.kthFactor(12, 5);
		System.out.println("6? " + factor);
	}
}
