package p20201209;


import java.util.Stack;

public class BSTIterator {
	static class NodeTrack {
		TreeNode treeNode; // contains left, right, val
		boolean needLeft = false;
		boolean needRight = false;
		boolean processed = false;
	}

	private final Stack<NodeTrack> stack;
	private int nextQueue;

	public BSTIterator(TreeNode root) {
		nextQueue = -1;
		stack = new Stack<>();

		NodeTrack track = new NodeTrack();
		track.treeNode = root;
		if(root.left != null) {
			track.needLeft = true;
		}
		if(root.right != null) {
			track.needRight = true;
		}
		stack.push(track);
	}

	public int next() {
		if(nextQueue != -1) {
			int rv = nextQueue;
			nextQueue = -1;
			return rv;
		}

		int val = -1;

		while(true) {
			if(stack.isEmpty()) {
				return -1;
			}

			NodeTrack node = stack.peek();
			if(node.needLeft) {
				// mark it as visited
				node.needLeft = false;
				NodeTrack track = new NodeTrack();
				track.treeNode = node.treeNode.left;
				if(track.treeNode.left != null) {
					track.needLeft = true;
				}
				if(track.treeNode.right != null) {
					track.needRight = true;
				}
				stack.push(track);
				continue;
			}

			if(!node.processed) {
				// in-order stuff
				node.processed = true;
				val = node.treeNode.val;
				break;
			}

			if(node.needRight) {
				node.needRight = false;
				NodeTrack track = new NodeTrack();
				track.treeNode = node.treeNode.right;
				if (track.treeNode.left != null) {
					track.needLeft = true;
				}
				if (track.treeNode.right != null) {
					track.needRight = true;
				}
				stack.push(track);
				continue;
			}
			stack.pop();
		}

		return val;
	}

	public boolean hasNext() {
		if(nextQueue == -1) {
			nextQueue = next();
			return nextQueue != -1;
		}
		return true;
	}
}
