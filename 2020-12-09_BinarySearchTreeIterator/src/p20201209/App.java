package p20201209;

public class App {
	public static void main(String[] args) {
		TreeNode treeNode = new TreeNode(
			7,
			new TreeNode(3),
			new TreeNode(15, new TreeNode(9), new TreeNode(20))
		);
		BSTIterator bSTIterator = new BSTIterator(treeNode);
		System.out.println("3? " + bSTIterator.next());    // return 3
		System.out.println("7? " + bSTIterator.next());    // return 7
		System.out.println("true? " + bSTIterator.hasNext()); // return True
		System.out.println("9? " + bSTIterator.next());    // return 9
		System.out.println("true? " + bSTIterator.hasNext()); // return True
		System.out.println("15? " + bSTIterator.next());    // return 15
		System.out.println("true? " + bSTIterator.hasNext()); // return True
		System.out.println("20? " + bSTIterator.next());    // return 20
		System.out.println("false? " + bSTIterator.hasNext()); // return False

		treeNode = new TreeNode(1);
		bSTIterator = new BSTIterator(treeNode);
		System.out.println("true? " + bSTIterator.hasNext()); // return false
		System.out.println("1? " + bSTIterator.next());    // return 1
		System.out.println("false? " + bSTIterator.hasNext()); // return false
	}
}
