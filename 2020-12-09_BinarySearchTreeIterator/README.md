## Binary Search Tree Iterator

Implement the `BSTIterator` class that represents an iterator over the **in-order traversal** of a binary search tree (BST):

- `BSTIterator(TreeNode root)` Initializes an object of the `BSTIterator` class. The `root` of the BST is given as part of the constructor. The pointer should be initialized to a non-existent number smaller than any element in the BST.
- `boolean hasNext()` Returns `true` if there exists a number in the traversal to the right of the pointer, otherwise returns `false`.
- `int next()` Moves the pointer to the right, then returns the number at the pointer.

Notice that by initializing the pointer to a non-existent smallest number, the first call to next() will return the smallest element in the BST.

You may assume that next() calls will always be valid. That is, there will be at least a next number in the in-order traversal when next() is called.


**Example 1:**


![bst-tree](images/bst-tree.png)
```
Input
["BSTIterator", "next", "next", "hasNext", "next", "hasNext", "next", "hasNext", "next", "hasNext"]
[[[7, 3, 15, null, null, 9, 20]], [], [], [], [], [], [], [], [], []]
Output
[null, 3, 7, true, 9, true, 15, true, 20, false]

Explanation
BSTIterator bSTIterator = new BSTIterator([7, 3, 15, null, null, 9, 20]);
bSTIterator.next();    // return 3
bSTIterator.next();    // return 7
bSTIterator.hasNext(); // return True
bSTIterator.next();    // return 9
bSTIterator.hasNext(); // return True
bSTIterator.next();    // return 15
bSTIterator.hasNext(); // return True
bSTIterator.next();    // return 20
bSTIterator.hasNext(); // return False
```


**Constraints:**

- The number of nodes in the tree is in the range `[1, 10^5]`.
- `0 <= Node.val <= 10^6`
- At most `10^5` calls will be made to `hasNext`, and `next`.
 

**Follow up:**

- Could you implement next() and hasNext() to run in average O(1) time and use O(h) memory, where h is the height of the tree?

---

## Solution

The best language for this would be python, since it contains a `yield` statement that is ideal for this type of question.

The simplest solution would be to traverse the entire tree and populate a list while traversing; then the `next()` and `hasNext()` methods become trivial. However, one of the follow-up requirements is violated with this approach, and in practical terms, is inefficient. Suppose a binary tree contains 1,000,000,000 nodes. Even before servicing any requests (next or hasNext), the entire tree has to be read created as a list, which doesn't use O(h) memory. Additionally, imagine that only 3 nodes of that tree needed to be read. This means that there was a massive waste of memory by reading everything that is not going to be requested by next().

My solution creates a wrapper for TreeNode so that metadata can be tracked. We cannot use recursion to traverse the tree since we do not intend on traversing the tree entirely before returning a result. Instead, we need to "pause" and "resume" for each node which a value is returned. A stack is ideal for this. At most, we will have _h_ items on the stack, since we only need to track the depth of our traversal.

This solution does not always deliver O(1) for `hasNext`. A fool-proof O(1) method is possible, but that will have to be revisited.
