## Convert Binary Number in a Linked List to Integer

Given `head` which is a reference node to a singly-linked list. The value of each node in the linked list is either 0 or 1. The linked list holds the binary representation of a number.

Return the _decimal value_ of the number in the linked list.

**Example 1:**

![graph1](images/graph-1.png "graph1")

```
Input: head = [1,0,1]
Output: 5
Explanation: (101) in base 2 = (5) in base 10
```

**Example 2:**

```
Input: head = [0]
Output: 0
```

**Example 3:**

```
Input: head = [1]
Output: 1
```

**Example 4:**

```
Input: head = [1,0,0,1,0,0,1,1,1,0,0,0,0,0,0]
Output: 18880
```

**Example 5:**

```
Input: head = [0,0]
Output: 0
```

**Constraints:**

- The Linked List is not empty.
- Number of nodes will not exceed 30.
- Each node's value is either 0 or 1.

---

## SOLUTION

The ListNode class is provided.

There is not much to analyze here. The process involves two steps until a null pointer in encountered:

- shift the bits of your accumulated number by 1 to the left
- traverse to the next node and add its value to your number

![output](images/output.png "output")
