package p20201101;

public class App {
	public static void main(String[] args) {
		Solution solution = new Solution();

		ListNode num1 = new ListNode(1, new ListNode(0, new ListNode(1, null)));
		System.out.println("[1,0,1] = " + solution.getDecimalValue(num1));

		ListNode num2 = new ListNode(0, null);
		System.out.println("[0] = " + solution.getDecimalValue(num2));

		ListNode num3 = new ListNode(1, null);
		System.out.println("[1] = " +solution.getDecimalValue(num3));

		ListNode num4 =
			new ListNode(1,
			new ListNode(0,
			new ListNode(0,
			new ListNode(1,
			new ListNode(0,
			new ListNode(0,
			new ListNode(1,
			new ListNode(1,
			new ListNode(1,
			new ListNode(0,
			new ListNode(0,
			new ListNode(0,
			new ListNode(0,
			new ListNode(0,
			new ListNode(0, null)))))))))))))));
		System.out.println("[1,0,0,1,0,0,1,1,1,0,0,0,0,0,0] = " + solution.getDecimalValue(num4));

		ListNode num5 = new ListNode(0, new ListNode(0, null));
		System.out.println("[0,0] = " + solution.getDecimalValue(num5));
	}
}
