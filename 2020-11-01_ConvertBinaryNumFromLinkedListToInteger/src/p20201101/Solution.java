package p20201101;

public class Solution {
	public int getDecimalValue(ListNode head) {
		int num = 0;
		ListNode ptr = head;
		while(ptr != null) {
			num <<= 1;
			num += ptr.val;
			ptr = ptr.next;
		}
		return num;
	}
}
