package p20201023;

import java.util.ArrayList;
import java.util.List;

public class Solution {
	private final List<LoHi> need = new ArrayList<>();

	static class LoHi {
		int lo;
		int hi;
	}

	public boolean find132pattern(int[] nums) {
		if(nums.length == 0) {
			return false;
		}
		need.clear();

		int low = nums[0], high = nums[0];

		for(int i=1 /* skip 0*/; i<nums.length; i++) {
			int num = nums[i]; // shorthand

			if(num >= high) { // >= saves me from a third condition (<, >, ==)
				high = num;
			} else if(num > low) { // num > low, num < high
				return true;
			} else {	// end of ascension ladder, and could satisfy some "required"
				if(high - low > 1) {
					LoHi loHi = new LoHi();
					loHi.lo = low;
					loHi.hi = high;
					need.add(loHi);
				}
				high = num;
				low = num;
			}

			if(need.size() > 0) {
				if (need.stream().anyMatch(it -> num > it.lo && num < it.hi)) {
					return true;
				}
			}
		}
		return false;
	}
}
