package p20201023;

public class App {
	public static void main(String[] args) {
		Solution solution = new Solution();
		int[] test1 = {1, 2, 3, 4};
		int[] test2 = {3, 1, 4, 2};
		int[] test3 = {-1, 3, 2, 0};
		int[] test4 = {3, 5, 0, 3, 4};
		int[] test5 = {-2, 1, 1};
		int[] test6 = {1, 4, 0, -1, -2, -3, -1, -2};
		int[] test7 = {1, 0, 1, -4, -3};

		System.out.println("false? " + solution.find132pattern(test1));
		System.out.println("true? " + solution.find132pattern(test2));
		System.out.println("true? " + solution.find132pattern(test3));
		System.out.println("true? " + solution.find132pattern(test4));
		System.out.println("false? " + solution.find132pattern(test5)); // false
		System.out.println("true? " + solution.find132pattern(test6));
		System.out.println("false? " + solution.find132pattern(test7)); // false
	}
}
