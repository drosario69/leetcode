## 132 Pattern

Given an array of `n` integers `nums`, a **132 pattern** is a subsequence of three integers `nums[i]`, `nums[j]` and `nums[k]`
such that `i < j < k` and `nums[i] < nums[k] < nums[j]`.

Return _`true` if there is a **132 pattern** in `nums`, otherwise, return `false`_.

**Follow up:** The `O(n^2)` is trivial, could you come up with the `O(n logn)` or the `O(n)` solution?

**Example 1:**

```
Input: nums = [1,2,3,4]
Output: false
Explanation: There is no 132 pattern in the sequence.
```

**Example 2:**

```
Input: nums = [3,1,4,2]
Output: true
Explanation: There is a 132 pattern in the sequence: [1, 4, 2].
```

**Example 3:**

``
Input: nums = [-1,3,2,0]
Output: true
Explanation: There are three 132 patterns in the sequence: [-1, 3, 2], [-1, 3, 0] and [-1, 2, 0].
``

**Constraints:**

- n == nums.length
- 1 <= n <= 10<sup>4</sup>
- -10<sup>9</sup> <= nums[i] <= 10<sup>9</sup>

---

## Solution

The `O(n)` solution dictates that we cannot iterate over the data set more than once.
My solution is, at worse, O(1.5n), and at best, O(3); if the first 3 values create a
low-high-mid pattern, we immediately return true.

My approach involves using "cliffs" to designate ranges. Consider the following sequence:

`1, 3, 5, 2`

You can visualize it as follows:

```
5     *
4    /|
3   / |
2  /  *
1 *
```

Begin with 1; that becomes the `low` value. Then we arrive at 3. That is now the
`high` value. We get to 5 and see that it is higher than `high` value, so 5 replaces
3 as the `high` value. We arrive at 2, which is lower than `high`. Before assuming that
we have found a low-high pair, perform subtraction. This eliminates contiguous numbers.
For example, if the low was 3 and the high was 4, we don't care about that hill/cliff
because we can't find a `medium` to fit between them, so we ignore them.

We store all of these hills/cliffs and compare present values against them. One thing
to note is the potential performance implication of:

```java
if(need.stream().anyMatch(it -> num > it.lo && num < it.hi)) {
	return true;
}
```

.anyMatch() can run in parallel, which is better than a linear search. However,
.stream() presents some overhead which negates the advantage of parallel searching.
The performance advantage would be realized on larger data sets.

---

## Output

![output](images/output.png "output")
