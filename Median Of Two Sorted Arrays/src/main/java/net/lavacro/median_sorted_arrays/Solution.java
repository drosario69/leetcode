package net.lavacro.median_sorted_arrays;

public class Solution {
	public double findMedianSortedArrays(int[] nums1, int[] nums2) {
		int max1 = nums1.length % 2 == 0 ? 2 : 1; // number of ints to use
		int max2 = nums2.length % 2 == 0 ? 2 : 1;

		/*
			This expression could be "(nums2.length % 2 == 0 ? 1 : 0)", but considering that max1 also does a modulus
			and the ternary operator assigns one more than we need, let's just use that value minus 1
		 */
		int pos1 = nums1.length / 2 - (max1 - 1);
		int pos2 = nums2.length / 2 - (max2 - 1);

		int[] sorted = new int[max1 + max2];

		int newpos = 0;

		System.out.printf("max1: %d, max2: %d, pos1: %d, pos2: %d%n", max1, max2, pos1, pos2);

		while(max1 + max2 > 0) {
			if(max1 < 0) {
				sorted[newpos++] = nums2[pos2++];
				max2--;
			} else if(max2 < 0) {
				sorted[newpos++] = nums1[pos1++];
				max1--;
			} else if(nums1[pos1] < nums2[pos2]) {
				sorted[newpos++] = nums1[pos1++];
				max1--;
			} else {
				sorted[newpos++] = nums2[pos2++];
				max2--;
			}
		}

		for(int num: sorted) {
			System.out.println(num);
		}
		return 0;
	}

	public static void main(String args[]) {
		Solution solution = new Solution();
		int[] arr1 = {1, 3};
		int[] arr2 = {2};
		System.out.println(solution.findMedianSortedArrays(arr1, arr2));
	}
}
