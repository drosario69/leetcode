package p20201117;

public class Solution {
	/*
		Receptors:

		bottom right = 0 (p, 0)
		top right = 1 (p, p)
		top left = 2 (0, p)
	 */
	public int mirrorReflection(int p, int q) {
		int x = p, y = q; // x will always either be 0 or p; we want y to be 0 or p
		boolean up = true; // the direction

		while(true) {
			if(y == 0 || y == p) {
				break;
			}

			if(up) {
				y += q;
				if(y > p) {
					y = p - (y - p);
					up = false;
				}
			} else {
				y -= q;
				if(y < 0) {
					y = Math.abs(y);
					up = true;
				}
			}
			x = Math.abs(x - p);

		}

		if(x == p && y == 0) {
			return 0;
		}
		if(x == p && y == p) {
			return 1;
		}
		return 2;
	}
}
