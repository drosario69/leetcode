package p20201117;

public class App {
	public static void main(String[] args) {
		Solution solution = new Solution();

		System.out.println("2? " + solution.mirrorReflection(2, 1));
		System.out.println("1? " + solution.mirrorReflection(3, 1));
		System.out.println("0? " + solution.mirrorReflection(3, 2));
	}
}
