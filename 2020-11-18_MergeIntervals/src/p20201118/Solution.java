package p20201118;

public class Solution {
	public int[][] merge(int[][] intervals) {
		int[][] clone = intervals.clone();

		for(int foo = 0; foo<intervals.length; foo++) {
			for (int i = 0; i < clone.length; i++) {
				for (int j = 0; j < clone.length; j++) {
					if (i == j) {
						continue;
					}
					if (clone[j][0] == -1) { // we already merged this
						continue;
					}
					if (
							!((clone[i][0] < clone[j][0] &&
									clone[i][1] < clone[j][0]) ||
									(clone[i][0] > clone[j][1] &&
											clone[i][1] > clone[j][1]))
					) {
						// contiguous
						if (clone[j][0] < clone[i][0]) { // second set starting point is less than first
							clone[i][0] = clone[j][0];
						}
						clone[j][0] = -1;

						if (clone[j][1] > clone[i][1]) { // second set starting point is less than first
							clone[i][1] = clone[j][1];
						}
						clone[j][1] = -1;
					}
				}
				clone = optimize(clone);
			}
		}

		return clone;
	}

	private int[][] optimize(int[][] orig) {
		int arrsize = 0;
		for(int[] line: orig) {
			if(line[0] != -1) {
				arrsize++;
			}
		}

		int[][] resp = new int[arrsize][2];
		arrsize = 0;
		for(int[] line: orig) {
			if(line[0] != -1) {
				resp[arrsize][0] = line[0];
				resp[arrsize][1] = line[1];
				arrsize++;
			}
		}
		return resp;
	}

	void disp(int[][] nums) {
		System.out.print("[");
		boolean started = false;
		for(int[] num: nums) {
			if(started) {
				System.out.print(", ");
			}
			System.out.print("[" + num[0] + "," + num[1] + "]");
			started = true;
		}
		System.out.println("]");
	}
}
