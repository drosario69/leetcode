package p20201118;

public class App {
	public static void main(String[] args) {
		Solution solution = new Solution();

		int[][] ques1 = { {1,3} ,{2,6}, {8,10}, {15,18}};
		int[][] ans1 = solution.merge(ques1);
		System.out.print("[[1,6], [8,10], [15,18]]? ");
		solution.disp(ans1);

		int[][] ques2 = {{1,4}, {4,5}};
		int[][] ans2 = solution.merge(ques2);
		System.out.print("[[1,5]]? ");
		solution.disp(ans2);

		int[][] ques3 = {{2,3}, {4,5}, {6,7}, {8,9}, {1,10}};
		int[][] ans3 = solution.merge(ques3);
		System.out.print("[[1,10]]? ");
		solution.disp(ans3);

		int[][] ques4 = {{3,5}, {0,0}, {4,4}, {0,2}, {5,6}, {4,5}, {3,5}, {1,3}, {4,6}, {4,6}, {3,4}};
		int[][] ans4 = solution.merge(ques4);
		System.out.print("[[0,6]]? ");
		solution.disp(ans4);
	}
}
