## Merge Intervals

Given an array of `intervals` where `intervals[i] = [start.i, end.i]`, merge all overlapping intervals, and return an _array of the non-overlapping intervals that cover all the intervals in the input._


**Example 1:**

```
Input: intervals = [[1,3],[2,6],[8,10],[15,18]]
Output: [[1,6],[8,10],[15,18]]
Explanation: Since intervals [1,3] and [2,6] overlaps, merge them into [1,6].
```

**Example 2:**

```
Input: intervals = [[1,4],[4,5]]
Output: [[1,5]]
Explanation: Intervals [1,4] and [4,5] are considered overlapping.
```
 
**Constraints:**

- 1 <= intervals.length <= 10<sup>4</sup>
- intervals[i].length == 2
- 0 <= start<sub>i</sub> <= end<sub>i</sub> <= 10<sup>4</sup>

---

## Solution

My approach is to have nested loops which merge and invalidate. For example:

`{{1,3}, {2,4}}`

Gets merged and invalidated to:

`{{1,4}, {-1,-1}}`

The invalidation (-1,-1) indicates we should skip this because its original contents were merge. After the loop, we then optimize the original list, shrinking the source data by removing all the (-1,-1) entries. This way, we can significantly cut down on future iterations.
