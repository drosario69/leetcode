package p20201120;

public class Solution {
	private int[] arr;
	private int search;

	public boolean search(final int[] nums, final int target) {
		if(nums.length == 0) {
			return false;
		}
		arr = nums;
		search = target;
		return locator(0, nums.length - 1);
	}

	private boolean locator(final int begin, final int end) {
		// quick tests
		if(end - begin == 0) {
			return search == arr[begin];
		}

		if(end - begin == 1) {
			return search == arr[begin] || search == arr[end];
		}

		int mid = (end + begin + 1) / 2; // +1 so that "end" becomes a candidate

		if(arr[mid] == search) { // found it!
			return true;
		}
		if(mid == begin) { // not found
			return false;
		}

		// for contiguous sections
		if(arr[begin] < arr[mid]) { // contiguous
			if(arr[begin] <= search && search <= arr[mid]) {
				return locator(begin, mid);
			} else { // it's on the right side
				return locator(mid, end);
			}
		} else if(arr[mid] < arr[end]) {
			if(arr[mid] <= search && search <= arr[end]) {
				return locator(mid, end);
			} else { // must be on the left side
				return locator(begin, mid);
			}
		}

		// brute force
		boolean left = locator(begin, mid);
		if(!left) {
			return locator(mid, end);
		} else {
			return true;
		}
	}
}
