package p20201120;

public class App {
	public static void main(String[] ags) {
		Solution solution = new Solution();

		int[] nums = {2, 5, 6, 0, 0, 1, 2};
		System.out.println("true? " + solution.search(nums, 0));

		System.out.println("false? " + solution.search(nums, 3));

		int[] nums2 = {1, 1, 3};
		System.out.println("true? " + solution.search(nums2, 3));

		int[] nums3 = {1, 3, 5};
		System.out.println("true? " + solution.search(nums3, 5));

		int[] nums4 = {1, 1, 1, 3, 1};
		System.out.println("true? " + solution.search(nums4, 3));
	}
}
