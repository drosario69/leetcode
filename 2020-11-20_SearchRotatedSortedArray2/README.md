## Search Rotated Sorted Array II

Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.

(i.e., `[0,0,1,2,2,5,6]` might become `[2,5,6,0,0,1,2]`).

You are given a target value to search. If found in the array return `true`, otherwise return `false`.

**Example 1:**

```
Input: nums = [2,5,6,0,0,1,2], target = 0
Output: true
```

**Example 2:**

```
Input: nums = [2,5,6,0,0,1,2], target = 3
Output: false
```

**Follow up:**

- This is a follow up problem to [Search in Rotated Sorted Array](https://leetcode.com/problems/search-in-rotated-sorted-array/description/), where `nums` may contain duplicates.
- Would this affect the run-time complexity? How and why?

---

The simplistic but effective approach is the following:

```java
class Solution {
    public boolean search(int[] nums, int target) {
        for(int num: nums) {
            if(num == target) {
                return true;
            }
        }
        return false;
    }
}
```

The follow-up asks about complexity, but there is no verbiage relating to efficiency. Had that been asked, the question
would be identical to the one asked in Daily Coding Problem, which I
[solved in C++](https://bitbucket.org/drosario69/daily-coding-problems/src/master/medium-problem_058/).

The approach operates on half of the data set, continually halving it until the target
is confirmed to exist or not exist. When we determine that a specific half does not
contain the pivot, we know whether that half is a candidate for containing the target.
For segments that contain the pivot, we don't know whether to search the left half
of that segment or the right half, so we search both. There may be a more intelligent
way to handle this.
