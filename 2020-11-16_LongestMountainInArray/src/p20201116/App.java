package p20201116;

public class App {
	public static void main(String[] args) {
		Solution solution = new Solution();

		int[] arr1 = {2, 1, 4, 7, 3, 2, 5};
		System.out.println("5? " + solution.longestMountain(arr1));

		int[] arr2 = {2, 2, 2};
		System.out.println("0? " + solution.longestMountain(arr2));

		int[] arr3 = {0, 1, 2, 3, 4, 5, 4, 3, 2, 1, 0};
		System.out.println("11? " + solution.longestMountain(arr3));

		int[] arr4 = {0, 1, 0};
		System.out.println("3? " + solution.longestMountain(arr4));

		int[] arr5 = {2, 0, 2, 0};
		System.out.println("3? " + solution.longestMountain(arr5));

		int[] arr6 = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
		System.out.println("0? " + solution.longestMountain(arr6));
	}
}
