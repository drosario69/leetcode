package p20201116;

public class Solution {
	public int longestMountain(int[] A) {
		if(A.length < 3) {
			return 0;
		}

		int prev = A[0];
		int delta = 1, max_width = 0;
		boolean incline = A[1] > prev; // make an assumption
		boolean was_uphill = false, dups = false;

		for(int i = 1; i<A.length; i++) {
			int num = A[i];

			if(num == prev) {
				dups = true;
			}

			if(num > prev) { // incline
				if(incline) {
					delta++;
				} else {
					if(delta > max_width && was_uphill && !dups) {
						max_width = delta;
					}
					incline = true;
					delta = 2;
					dups = false;
				}
			} else {
				delta++;
				if(incline) { // hit a peak, now coming down
					incline = false;
					was_uphill = true;
				}
				if(delta > max_width && was_uphill && !dups) {
					max_width = delta;
				}
			}
			prev = num;

		}

		return max_width;
	}
}
