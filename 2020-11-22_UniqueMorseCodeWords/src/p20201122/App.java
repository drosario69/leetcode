package p20201122;

public class App {
	public static void main(String[] args) {
		Solution solution = new Solution();

		String[] morse = {"gin", "zen", "gig", "msg"};
		int unique = solution.uniqueMorseRepresentations(morse);
		System.out.println("Unique: " + unique);
	}
}
