package p20201122;

import java.util.HashSet;

public class Solution {
	private static final String[] MORSE = {
			".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", // a-m
			"-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.." // n-z
	};

	public int uniqueMorseRepresentations(String[] words) {
		StringBuilder sb = new StringBuilder();
		HashSet<String> code = new HashSet<>();

		for(String word: words) {
			sb.setLength(0);
			for(int i = 0; i < word.length(); i++) {
				char c = word.charAt(i);
				sb.append(MORSE[c - 'a']);
			}
			code.add(sb.toString());
		}
		return code.size();
	}
}
