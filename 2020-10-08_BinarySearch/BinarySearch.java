class Solution {
	public int search(int[] nums, int target) {
		int pivot, left = 0, right = nums.length - 1;
		while (left <= right) {
			pivot = left + (right - left) / 2;
			if (nums[pivot] == target) {
				return pivot;
			}
			if (target < nums[pivot]) {
				right = pivot - 1;
			} else {
				left = pivot + 1;
			}
		}
		return -1;
	}
}

public class BinarySearch {
	public static void main(String[] args) {
		int[] arr1 = {-1, 0, 3, 5, 9, 12};
		Solution solution = new Solution();
		System.out.println(solution.search(arr1, 9));

		int[] arr2 = {-1, 0, 3, 5, 9, 12};
		System.out.println(solution.search(arr2, 2));

		int[] arr3 = {2, 5};
		System.out.println(solution.search(arr3, 5));
	}
}
